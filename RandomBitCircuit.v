module RandomBitCircuit
/*
	Module accepts static set of bits, then shifts set number of times
	to output a random number from xor'ing bits 13 times
*/
(
	input wire clk, reset, masterTick,
    output reg [1:0] rnd 
);

parameter [3:0] randomAssign = 0,
				randomCalc = 1,
				randomGen = 2;

wire feedback = random[12] ^ random[3] ^ random[2] ^ random[0]; 

reg [12:0] random, random_next, random_done;
reg [3:0] stateReg, stateNext, count, count_next; //to keep track of the shifts

always@(posedge clk or posedge reset)
begin
	if (reset)
	begin
		random <= 0; //An LFSR cannot have an all 0 state, thus reset to FF
		count <= 0;
		stateReg <= 0;
	end

	else
	begin
		random <= random_next;
		count <= count_next;
		stateReg <= stateNext;
	end
end

always@*
begin
	random_next = random; //default state stays the same
	count_next = count;
	stateNext = stateReg;
	case(stateReg)
		randomAssign:
		begin
			random_next = 13'h9;
			stateNext = randomCalc;
		end
		randomCalc:
		begin
			random_next = {random[11:0], feedback}; //shift left the xor'd every posedge clock
			count_next = count + 1;
			if (count == 13)
			begin
				count_next = 0;
				random_done = random; //assign the random number to output after 13 shifts
				stateNext = randomGen;
			end
		end
		randomGen:
		begin
		if(masterTick)
			begin
				random_done = {random_done[0], random_done[12:1]};
				rnd = random_done[1:0];
			end
		end
	endcase
end

endmodule


