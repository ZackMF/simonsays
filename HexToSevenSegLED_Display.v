module HexToSevenSegLED_Display
(
	input wire [3:0] a,
	output reg [6:0] f
);

always@*
	begin
		case(a)
			4'h0: f = 7'b1000000;
			4'h1: f = 7'b1111001;
			4'h2: f = 7'b0100100;
			4'h3: f = 7'b0110000;
			4'h4: f = 7'b0011001;
			4'h5: f = 7'b0010010;
			4'h6: f = 7'b0000010;
			4'h7: f = 7'b1111000;
			4'h8: f = 7'b0000000;
			4'h9: f = 7'b0010000;
			4'ha: f = 7'b0100000;
			4'hb: f = 7'b0000011;
			4'hc: f = 7'b1000110;
			4'hd: f = 7'b0100001;
			4'he: f = 7'b0000100;
			default: f = 7'b0001110;
		endcase
	end
endmodule