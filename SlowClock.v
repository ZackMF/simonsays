module SlowClock
/*
	Module accepts counter and slows hardware clock / counter
	Output is slowed clock tick
*/
(
	input wire clk, reset,
	input wire [27:0] counterIn,
	output reg clkOut
);

reg [27:0] counter;

always@(posedge reset or posedge clk)
begin
	if(reset == 1'b1)
		begin
			clkOut <= 0;
			counter <= 0;
		end
	else
		begin
			counter <= (counter + 1);
			if(counter == counterIn)
				begin
					counter <= 0;
					clkOut <= ~clkOut;
				end
		end
end

endmodule
