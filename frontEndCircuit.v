module frontEndCircuit
(
	input wire clk, reset, start,
	//Button inputs
	input wire [3:0] KEY,
	//Green LED outputs
	output wire [7:0] LEDG,
	//HEX display outputs
	output reg [6:0] HEX7, HEX6, HEX3, HEX2, HEX1, HEX0
);

//Wire to innerconnect modules
wire masterTick;
wire [1:0] randTemp;
wire [3:0] hexInLvl, hexInState, hexInP, ledOut;
wire [6:0] hexOutLvl, hexOutState, hexOutP;
wire [27:0] counterWire;

//Main module for game calculation
SimonSaysCircuit mainCircuit (.clk(clk), .masterTick(masterTick), .reset(reset), .start(start), .buttonIn(~KEY), 
.randomNum(randTemp), .pfOut(pfOut), .stateC(hexInState), .ledOut(LEDG), 
.counterOut(counterWire), .lvlOut(hexInLvl), .patternOut(hexInP));

RandomBitCircuit randomBit (.clk(clk), .reset(reset), .masterTick(masterTick), .rnd(randTemp));

//Clock to slow main module
SlowClock clkControl (.clk(clk), .reset(reset), .counterIn(counterWire), .clkOut(masterTick));

//Level outHex, state outHex, pattern outHex
HexToSevenSegLED_Display calcHEX (.a(hexInLvl), .f(hexOutLvl));
HexToSevenSegLED_Display patternHEX (.a(hexInP), .f(hexOutP));
HexToSevenSegLED_Display stateHex (.a(hexInState), .f(hexOutState));

always@*
begin
	//Button LED activation via main circuit from right to left
	
	//Display current level, pass, or lose
	case(pfOut)
	//Current Level / Default State
	2'b00:
	begin
		HEX3 = 7'b1000110;
		HEX2 = 7'b1000111;
		HEX1 = 7'b0111111;
		HEX0 = hexOutLvl;
	end
	//Pass
	2'b01:
	begin
		HEX3 = 7'b0001100;
		HEX2 = 7'b0001000;
		HEX1 = 7'b0010010;
		HEX0 = 7'b0010010;
	end
	//Lose
	2'b10:
	begin
		HEX3 = 7'b1000110;
		HEX2 = 7'b1000000;
		HEX1 = 7'b0010010;
		HEX0 = 7'b0000110;
	end
	2'b11:
	begin
		HEX3 = 7'b1000110;
		HEX2 = 7'b1000110;
		HEX1 = 7'b1000110;
		HEX0 = 7'b1000110;
	end
	endcase
	
	//Debug HEXex = Current Pattern, Current State
	HEX6 = hexOutP;
	HEX7 = hexOutState;
end

endmodule
