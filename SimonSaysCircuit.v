module SimonSaysCircuit
(
	//SlowClock in, reset in, start switch in
	input wire clk, masterTick, reset, start,
	//4 Button input to main
	input wire [3:0] buttonIn,
	//Random 2 bit number in
	input wire [1:0] randomNum,
	//Pass Fail 2 bit number out
	output reg [1:0] pfOut,
	//LED Integer out BCD
	output reg [7:0] ledOut,
	//SlowClock counter out to module to change masterTick rate
	output reg [27:0] counterOut,
	//Debug out, Current state, Current level, Current Pattern Count
	output wire [3:0] stateC, lvlOut, patternOut
);

//States
parameter [3:0] idle = 0,
				compAssign = 1,
				computerCompute = 2,
				userInput = 3,
				check = 4;

//pf is pass fail tracker
reg [3:0] stateReg, stateNext, levelReg, levelNext,
patternCount, patternNext;
reg [23:0] inputReg, inputNext, outputReg,  outputNext;
//HARD CODE FOR HARD!
reg [1:0] rnd;
reg [12:0] randomReg, randomNext;

//Assign Debug outputs
assign stateC = stateReg;
assign lvlOut = levelReg;
assign patternOut = patternCount;

//Data Register change dependant on SlowClock module and counter
always@(posedge masterTick, posedge reset)
begin
	if(reset)
		begin
			stateReg <= 0;
			levelReg <= 0;
			inputReg <= 0;
			outputReg <= 0;
			patternCount <= 0;
		end
	else
		begin
			stateReg <= stateNext;
			levelReg <= levelNext;
			inputReg <= inputNext;
			outputReg <= outputNext;
			patternCount <= patternNext;
			randomReg <= randomNext;
		end
end

always@(posedge masterTick)
begin
	case(stateReg)
		idle:
		begin
			//Set default values before start of next game = default game state.
			pfOut = 2'b00;
			ledOut = 8'b00000000;
			 //Cannot be within idle state
			if(start)
			begin
				stateNext = compAssign;
				randomNext = 12'b101001110100;
			end
		end
		compAssign:
		begin
			if(start)
			begin
				stateNext = computerCompute;
				levelNext = (levelReg + 1);
				patternNext = (levelReg + 2);
				counterOut = 20_000_000;
			end
		end
		//Computer pattern determined and saved
		computerCompute:
		begin
			if(start)
				begin
					ledOut = 8'b00000000;
					//Slow clock down for computer output
					if(patternCount != 0)
						begin
							//N = N + 2;
							case(randomNum)
							2'b00: 
								begin
								outputNext = ({outputNext, randomNum});
								patternNext = (patternCount - 1);
								ledOut = 8'b00000011;
								end
							2'b01:
								begin
								outputNext = ({outputNext, randomNum});
								patternNext = (patternCount - 1);
								ledOut = 8'b00001100;
								end
							2'b10:
								begin
								outputNext = ({outputNext, randomNum});
								patternNext = (patternCount - 1);
								ledOut = 8'b00110000;
								end
							2'b11:
								begin
								outputNext = ({outputNext, randomNum});
								patternNext = (patternCount - 1);
								ledOut = 8'b11000000;
								end
							endcase
						end
					else
						begin
							counterOut = 5_000_000;
							patternNext = (levelReg + 2);
							stateNext = userInput;
						end
				end
		end
		//User button inputs
		userInput:
		begin
			if(start)
			begin
				ledOut = 8'b00000000;
				if(patternCount != 0)
				begin
					case(buttonIn)
						4'b0001: 
							begin
							inputNext = ({inputReg, 2'b00});
							patternNext = (patternCount - 1);
							//stateNext = check;
							ledOut = 8'b00000011;
							end
						4'b0010:
							begin
							inputNext = ({inputReg, 2'b01});
							patternNext = (patternCount - 1);
							//stateNext = check;
							ledOut = 8'b00001100;
							end
						4'b0100:
							begin
							inputNext = ({inputReg, 2'b10});
							patternNext = (patternCount - 1);
							//stateNext = check;
							ledOut = 8'b00110000;
							end
						4'b1000:
							begin
							inputNext = ({inputReg, 2'b11});
							patternNext = (patternCount - 1);
							//stateNext = check;
							ledOut = 8'b11000000;
							end
						default: inputNext = inputReg;
					endcase
				end
				else
					begin
						stateNext = check;
						counterOut = 10_000_000;
					end
			end
		end
		//Check for fail, pass if pattern = 0
		check:
		begin
			if(start)
			begin
				if(outputReg != inputReg)
					begin
						pfOut = 2'b01;
						//levelNext = 0;
						stateNext = idle;
						counterOut = 50_000_000;
						//output fail in 7 segment
					end
			end
		end
	endcase
end

endmodule
	